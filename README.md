## compile and install
```
make
sudo make install
```

## run test
```
make test
```

The tests mock some libraries like libfwinterface. The tests won't add actual
rules so there is no need for increased permissions. Lib_fwrules isn't mocked because it is important
to guarantee a stable plugin behavior. If tests start to fail because of a change `gdb` is
a good tool. The tests are organized per topic (Policy, Service, ...) and a breakpoint can be added
at the topic's event handler to start debugging.
```
cd test/path/to/test
gdb ./run_test
```

## run plugin inside a Ambiorix Docker container

Refer to the Ambiorix tutorials for the steps to launch the Ambiorix Docker container.

### compile/install dependencies: (make + sudo make install)

mod-fw-host:
  libfwrules
  libfwinterface
    libiptc (sudo apt install libiptc-dev)
  libipat
  libmnl (sudo apt install libmnl-dev)
  libgmap-client