/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FW_HOST_RULES_H__)
#define __FW_HOST_RULES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>

typedef enum {
    FW_NEW = 1,
    FW_DELETED = 2,
    FW_MODIFIED = 4,
} flags_t;

typedef enum {
    FIREWALL_ENABLED,
    FIREWALL_DISABLED,
    FIREWALL_PENDING,
    FIREWALL_ERROR_MISCONFIG,
    FIREWALL_ERROR
} status_t;

typedef enum {
    FIREWALL_TARGET_DROP,
    FIREWALL_TARGET_ACCEPT,
    FIREWALL_TARGET_CHAIN,
    FIREWALL_TARGET_REJECT,
    FIREWALL_TARGET_RETURN,
    FIREWALL_TARGET_REDIRECT,
    FIREWALL_TARGET_NONE
} target_t;

typedef struct {
    status_t status;
    fw_folder_t* folder;
    fw_folder_t* folder6;
} common_t;

bool fw_host_implement_changes(amxc_htable_t* ht_fw_host);
bool fw_host_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag, const char* chain, const char* table, int index);
void fw_host_delete_common_rules(common_t* common);

#ifdef __cplusplus
}
#endif

#endif // __FW_HOST_RULES_H__
