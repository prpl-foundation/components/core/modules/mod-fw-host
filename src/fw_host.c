/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#include <gmap/gmap.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "fw_host.h"
#include "fw_host_rules.h"
#include "fw_gmap_device.h"
#include "fw_host_dm.h"

// module trace zone
#define ME DEFAULT_TRACE_ZONE_NAME

static amxo_parser_t* fw_parser = NULL;
static amxd_dm_t* fw_dm = NULL;
static amxb_bus_ctx_t* gmap_ctx = NULL;
static gmap_query_t* query = NULL;
static amxp_timer_t* timer_retry_start = NULL;

amxo_parser_t* fw_get_parser(void) {
    return fw_parser;
}

static void fw_query_callback(UNUSED gmap_query_t* lquery, const cstring_t key, amxc_var_t* device, gmap_query_action_t action) {
    SAH_TRACEZ_IN(ME);

    if(action == gmap_query_expression_start_matching) {
        add_gmap_device(key, device);
        add_device_instance(fw_dm, fw_parser, key, device);
    } else if(action == gmap_query_device_updated) {
        add_gmap_device(key, device);
        change_device_instance(fw_dm, fw_parser, key, device);
    } else if(action == gmap_query_expression_stop_matching) {
        delete_gmap_device(key);
        delete_device_instance(fw_dm, fw_parser, key);
    }

    SAH_TRACEZ_OUT(ME);
    return;
}

static int open_gmap_query(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    gmap_client_init(gmap_ctx);

    query = gmap_query_open_ext("lan and not self and physical and wanblock", "Firewall", fw_query_callback, NULL);
    when_null_trace(query, exit, ERROR, "Failed to open Gmap query Firewall");

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void continue_initialization(UNUSED amxp_timer_t* timer, UNUSED void* data) {
    SAH_TRACEZ_IN(ME);
    int ret_value = -1;

    gmap_ctx = amxb_be_who_has("Devices.Device.");
    //Sometimes it hangs (5% of the times when booting up), even with a signal from amxb. This is a workaround a nd must be removed in the future.
    if((gmap_ctx == NULL) && (timer_retry_start == NULL)) {
        SAH_TRACEZ_ERROR(ME, "No Gmap bus context found - retrying again in 3 seconds.");
        ret_value = amxp_timer_new(&timer_retry_start, continue_initialization, NULL);
        when_failed_trace(ret_value, exit, ERROR, "Failed to initialize the timer, the module will stop.");
        ret_value = amxp_timer_start(timer_retry_start, 3000);
        when_failed_trace(ret_value, exit, ERROR, "Failed to start the timer, the module will stop.");
        goto rearm;
    }
    when_null_trace(gmap_ctx, exit, ERROR, "No Gmap bus context found. The module will stop.");

    ret_value = open_gmap_query();
    when_failed(ret_value, exit);

exit:
    amxp_timer_delete(&timer_retry_start);
rearm:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void wait_for_gmap_cb(UNUSED const char* signame,
                             UNUSED const amxc_var_t* const data,
                             UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    continue_initialization(NULL, NULL);
    SAH_TRACEZ_OUT(ME);
    return;
}

static int fw_host_start(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxd_object_t* fw_obj = NULL;

    when_null_trace(parser, exit, ERROR, "The parser pointer is NULL.");
    when_null_trace(dm, exit, ERROR, "The datamodel pointer is NULL.");
    fw_parser = parser;
    fw_dm = dm;

    // Add dependency to Firewall plugin as it is a firewall exclusive module
    fw_obj = amxd_dm_findf(dm, "Firewall.");
    when_null_trace(fw_obj, exit, ERROR, "Failed to find the Firewall object.");

    gmap_ctx = amxb_be_who_has("Devices.Device.");
    if(gmap_ctx == NULL) {
        amxb_wait_for_object("Devices.Device.");
        ret_value = amxp_slot_connect_filtered(NULL, "^wait:Devices\\.Device\\.$", NULL, wait_for_gmap_cb, NULL);
        when_failed_trace(ret_value, exit, ERROR, "Failed to wait for Gmap to be available");
        SAH_TRACEZ_WARNING(ME, "Waiting for Gmap");
        rv = 1;
        goto exit;
    }

    ret_value = open_gmap_query();
    when_failed(ret_value, exit);

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int fw_host_stop(void) {
    SAH_TRACEZ_IN(ME);

    clean_all_gmap_devices();
    fw_host_clean_all_instances(fw_dm, fw_parser);
    gmap_query_close(query);
    amxp_timer_delete(&timer_retry_start);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

int _fw_host_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int retval = 0;
    switch(reason) {
    case AMXO_START:
        retval = fw_host_start(dm, parser);
        if(retval == 1) {
            SAH_TRACEZ_ERROR(ME, "Module is waiting for Gmap to continue the initialization.");
        } else if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to start module fw_host");
        } else {
            SAH_TRACEZ_INFO(ME, "Module fw_host started");
        }
        break;
    case AMXO_STOP:
        fw_host_stop();
        SAH_TRACEZ_INFO(ME, "Module fw_host stopped");
        break;
    }
    SAH_TRACEZ_OUT(ME);
    return 0;
}
