/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "fw_host.h"
#include "fw_host_dm.h"

// module trace zone
#define ME DEFAULT_TRACE_ZONE_NAME

const char* fw_get_vendor_prefix(amxo_parser_t* parser) {
    amxc_var_t* setting = amxo_parser_get_config(parser, "prefix_");
    const char* prefix = amxc_var_constcast(cstring_t, setting);
    return (prefix != NULL) ? prefix : "";
}

static amxd_object_t* wan_access_template(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxd_object_t* templ = NULL;
    templ = amxd_dm_findf(dm, "Firewall.%s%s.", fw_get_vendor_prefix(parser), DM_OBJ_WAN_ACCESS);
    if(templ == NULL) {
        templ = amxd_dm_findf(dm, "Firewall.%s.", DM_OBJ_WAN_ACCESS);
    }
    return templ;
}

static amxd_object_t* block_list_template(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxd_object_t* templ = NULL;
    amxd_object_t* wan_access_templ = NULL;

    wan_access_templ = wan_access_template(dm, parser);
    when_null(wan_access_templ, exit);

    templ = amxd_object_findf(wan_access_templ, ".%s.", DM_BLOCK_LIST);

exit:
    return templ;
}

static amxd_trans_t* fw_host_transaction_create(amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t* trans = NULL;
    bool clean_trans = true;

    when_null(object, exit);
    when_failed(amxd_trans_new(&trans), exit);
    when_failed(amxd_trans_set_attr(trans, amxd_tattr_change_ro, true), clean);
    when_failed(amxd_trans_select_object(trans, object), clean);

    clean_trans = false;
clean:
    if(clean_trans == true) {
        amxd_trans_delete(&trans);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

static int set_param_from_device_to_dm(amxd_trans_t* transaction, amxc_var_t* device, const cstring_t key) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const cstring_t tmp_str = NULL;

    when_null_trace(transaction, exit, ERROR, "The transaction is NULL.");
    when_null_trace(device, exit, ERROR, "The device is NULL.");
    when_str_empty_trace(key, exit, ERROR, "The key is an empty string.");

    tmp_str = GET_CHAR(device, key);
    amxd_trans_set_value(cstring_t, transaction, key, tmp_str != NULL ? tmp_str : "");

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int copy_params_from_device_to_dm(amxd_trans_t* transaction, amxc_var_t* device) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_MAC), exit);
    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_IP), exit);
    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_IP_SRC), exit);
    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_INTF), exit);
    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_SRC), exit);
    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_TYPE), exit);
    when_failed(set_param_from_device_to_dm(transaction, device, DM_BLOCK_LIST_TAGS), exit);

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int add_device_instance(amxd_dm_t* dm, amxo_parser_t* parser, const cstring_t key, amxc_var_t* device) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxd_trans_t* transaction = NULL;
    amxd_object_t* block_list_templ = NULL;
    amxd_object_t* block_list_inst = NULL;

    when_str_empty_trace(key, exit, ERROR, "The key is an empty string.");
    when_null_trace(device, exit, ERROR, "The device variant is NULL.");

    block_list_templ = block_list_template(dm, parser);
    when_null_trace(block_list_templ, exit, ERROR, "Failed to get the WANAccess.BlockList template.");
    block_list_inst = amxd_object_findf(block_list_templ, ".%s.", key);
    if(block_list_inst != NULL) {
        ret_value = change_device_instance(dm, parser, key, device);
        when_failed(ret_value, exit)
    } else {
        transaction = fw_host_transaction_create(block_list_templ);
        when_null_trace(transaction, exit, ERROR, "Failed to create the transaction.");
        ret_value = amxd_trans_add_inst(transaction, 0, key);
        when_failed_trace(ret_value, exit, ERROR, "Failed to add an instance.");
        ret_value = copy_params_from_device_to_dm(transaction, device);
        when_failed_trace(ret_value, exit, ERROR, "Failed to copy the information from the variant to the data model.");
        ret_value = amxd_trans_apply(transaction, dm);
        when_failed_trace(ret_value, exit, ERROR, "Failed to apply the transaction.");
    }

    rv = 0;
exit:
    amxd_trans_delete(&transaction);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int change_device_instance(amxd_dm_t* dm, amxo_parser_t* parser, const cstring_t key, amxc_var_t* device) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxd_object_t* block_list_templ = NULL;
    amxd_object_t* block_list_inst = NULL;
    amxd_trans_t* transaction = NULL;

    when_str_empty_trace(key, exit, ERROR, "The key is an empty string.");
    when_null_trace(device, exit, ERROR, "The device variant is NULL.");

    block_list_templ = block_list_template(dm, parser);
    when_null_trace(block_list_templ, exit, ERROR, "Failed to get the WANAccess.BlockList template.");
    block_list_inst = amxd_object_findf(block_list_templ, ".%s.", key);
    when_null_trace(block_list_inst, exit, ERROR, "Failed to get the WANAccess.BlockList.%s. object", key);

    transaction = fw_host_transaction_create(block_list_inst);
    when_null_trace(transaction, exit, ERROR, "Failed to create the transaction.");

    ret_value = copy_params_from_device_to_dm(transaction, device);
    when_failed_trace(ret_value, exit, ERROR, "Failed to copy the information from the variant to the data model.");

    ret_value = amxd_trans_apply(transaction, dm);
    when_failed_trace(ret_value, exit, ERROR, "Failed to apply the transaction.");

    rv = 0;
exit:
    amxd_trans_delete(&transaction);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int delete_device_instance(amxd_dm_t* dm, amxo_parser_t* parser, const cstring_t key) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxd_trans_t* transaction = NULL;
    amxd_object_t* block_list_templ = NULL;
    amxd_object_t* block_list_inst = NULL;

    when_str_empty_trace(key, exit, ERROR, "The key is an empty string.");

    block_list_templ = block_list_template(dm, parser);
    when_null_trace(block_list_templ, exit, ERROR, "Failed to get the WANAccess.BlockList template.");
    block_list_inst = amxd_object_findf(block_list_templ, ".%s.", key);
    when_null_trace(block_list_inst, exit, ERROR, "Failed to get the WANAccess.BlockList.%s. object", key);

    transaction = fw_host_transaction_create(block_list_templ);
    when_null_trace(transaction, exit, ERROR, "Failed to create the transaction.");

    amxd_trans_del_inst(transaction, 0, key);

    ret_value = amxd_trans_apply(transaction, dm);
    when_failed_trace(ret_value, exit, ERROR, "Failed to apply the transaction.");

    rv = 0;
exit:
    amxd_trans_delete(&transaction);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void fw_host_clean_all_instances(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int ret_value = -1;
    amxd_object_t* block_list_templ = NULL;
    amxd_object_t* block_list_inst = NULL;
    amxd_trans_t* transaction = NULL;

    block_list_templ = block_list_template(dm, parser);
    when_null_trace(block_list_templ, exit, ERROR, "Failed to get the WANAccess.BlockList template.");

    transaction = fw_host_transaction_create(block_list_templ);
    when_null_trace(transaction, exit, ERROR, "Failed to create the transaction.");

    amxd_object_for_each(instance, it, block_list_templ) {
        block_list_inst = amxc_container_of(it, amxd_object_t, it);
        amxd_trans_del_inst(transaction, 0, amxd_object_get_name(block_list_inst, AMXD_OBJECT_NAMED));
    }

    ret_value = amxd_trans_apply(transaction, dm);
    when_failed_trace(ret_value, exit, ERROR, "Failed to apply the transaction.");

exit:
    amxd_trans_delete(&transaction);
    SAH_TRACEZ_OUT(ME);
    return;
}