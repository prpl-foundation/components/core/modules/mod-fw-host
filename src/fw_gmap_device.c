/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "fw_host.h"
#include "fw_host_rules.h"
#include "fw_gmap_device.h"

// module trace zone
#define ME DEFAULT_TRACE_ZONE_NAME

static amxc_htable_t* ht_fw_host = NULL;

static fw_gmap_device_t* create_gmap_device(amxc_var_t* var) {
    fw_gmap_device_t* tmp = NULL;
    const char* mac_address = NULL;

    when_null_trace(var, exit, ERROR, "The variant is NULL.");
    mac_address = GET_CHAR(var, "PhysAddress");
    when_str_empty_trace(mac_address, exit, ERROR, "There is no MAC address in the Device's variant.");

    tmp = calloc(1, sizeof(fw_gmap_device_t));
    amxc_string_init(&(tmp->mac_addr), 0);
    amxc_string_setf(&(tmp->mac_addr), "%s", mac_address);
    amxc_var_init(&(tmp->device));
    amxc_var_set_type(&(tmp->device), AMXC_VAR_ID_HTABLE);
    amxc_var_copy(&(tmp->device), var);
    fw_folder_new(&tmp->common.folder);
    fw_folder_new(&tmp->common.folder6);
    tmp->flags |= FW_NEW;
    tmp->common.status = FIREWALL_DISABLED;

exit:
    return tmp;
}

static int change_gmap_device(fw_gmap_device_t* gmap_device, amxc_var_t* var) {
    int rv = -1;
    const char* mac_address = NULL;

    when_null_trace(gmap_device, exit, ERROR, "The GMAP device is NULL.");
    when_null_trace(var, exit, ERROR, "The variant is NULL.");
    mac_address = GET_CHAR(var, "PhysAddress");
    when_str_empty_trace(mac_address, exit, ERROR, "There is no MAC address in the Device's variant.");

    amxc_string_setf(&(gmap_device->mac_addr), "%s", mac_address);
    amxc_var_copy(&(gmap_device->device), var);
    gmap_device->flags |= FW_MODIFIED;

    rv = 0;
exit:
    return rv;
}



static int clean_gmap_device(fw_gmap_device_t* gmap_device) {
    int rv = -1;

    when_null_trace(gmap_device, exit, ERROR, "The GMAP device is NULL.");
    fw_host_delete_common_rules(&(gmap_device->common));
    amxc_string_clean(&(gmap_device->mac_addr));
    amxc_var_clean(&(gmap_device->device));
    amxc_htable_it_clean(&gmap_device->it, NULL);
    free(gmap_device);
    gmap_device = NULL;

    rv = 0;
exit:
    return rv;
}

/**
   @brief
   Allocate the memory and set default values of a Gmap device
   internal structure.

   @param[in] key Key of the device to be added.
   @param[in] var Hash table of the device.

   @return
   0 if success, 1 if failed.
 */
int add_gmap_device(const cstring_t key, amxc_var_t* var) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    amxc_htable_it_t* it_device = NULL;
    fw_gmap_device_t* gmap_device = NULL;

    when_str_empty_trace(key, exit, ERROR, "The key is empty.");
    when_null_trace(var, exit, ERROR, "The variant is NULL.");

    if(ht_fw_host == NULL) {
        amxc_htable_new(&ht_fw_host, 1);
    }

    it_device = amxc_htable_get(ht_fw_host, key);
    if(it_device == NULL) {
        gmap_device = create_gmap_device(var);
        when_null_trace(gmap_device, exit, ERROR, "Failed to create the gmap device struct.");
        amxc_htable_insert(ht_fw_host, key, &gmap_device->it);
    } else {
        gmap_device = amxc_htable_it_get_data(it_device, fw_gmap_device_t, it);
        when_null_trace(gmap_device, exit, ERROR, "Failed to retrieve the gmap device struct.");
        ret_value = change_gmap_device(gmap_device, var);
        when_failed_trace(ret_value, exit, ERROR, "Failed to change the gmap device struct.");
    }

    when_false_trace(fw_host_implement_changes(ht_fw_host), exit, ERROR, "Failed to implement rules.");


    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Cleanup the Gmap device.

   @param[in] key Key of the device to be removed.

   @return
   0 if success, 1 if failed.
 */
int delete_gmap_device(const cstring_t key) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int ret_value = -1;
    fw_gmap_device_t* gmap_device = NULL;

    when_null_trace(ht_fw_host, exit, ERROR, "The devices' hash table is NULL.");
    when_str_empty_trace(key, exit, ERROR, "The key is empty.");

    gmap_device = get_gmap_device(key);
    when_null_trace(gmap_device, exit, ERROR, "The device with the key - '%s' doesn't exist.", key);
    gmap_device->flags |= FW_DELETED;
    when_false_trace(fw_host_implement_changes(ht_fw_host), exit, ERROR, "Failed to implement rules.");

    ret_value = clean_gmap_device(gmap_device);
    when_failed_trace(ret_value, exit, ERROR, "Failed to clean the gmap device struct.")

    if(amxc_htable_is_empty(ht_fw_host)) {
        clean_all_gmap_devices();
    }

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Gets the Device variant from the internal hash table.

   @param[in] key Key of the device to be added.

   @return
   Gmap device, NULL if failed.
 */
fw_gmap_device_t* get_gmap_device(const cstring_t key) {
    SAH_TRACEZ_IN(ME);
    fw_gmap_device_t* device = NULL;

    when_str_empty_trace(key, exit, ERROR, "The key is empty.");
    when_null_trace(ht_fw_host, exit, ERROR, "The devices' hash table is NULL.");

    device = amxc_htable_it_get_data(amxc_htable_get(ht_fw_host, key), fw_gmap_device_t, it);
exit:
    SAH_TRACEZ_OUT(ME);
    return device;
}

/**
   @brief
   Clean all the Gmap devices.

 */
void clean_all_gmap_devices(void) {
    SAH_TRACEZ_IN(ME);

    amxc_htable_for_each(it_device, ht_fw_host) {
        fw_gmap_device_t* gmap_device = amxc_htable_it_get_data(it_device, fw_gmap_device_t, it);
        clean_gmap_device(gmap_device);
    }
    amxc_htable_delete(&ht_fw_host, NULL);
    ht_fw_host = NULL;

    SAH_TRACEZ_OUT(ME);
    return;
}