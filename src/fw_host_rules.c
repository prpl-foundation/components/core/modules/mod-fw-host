/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <fwinterface/interface.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "fw_host.h"
#include "fw_host_rules.h"
#include "fw_gmap_device.h"

// module trace zone
#define ME DEFAULT_TRACE_ZONE_NAME

static const char* fw_get_chain(const char* search_path, const char* default_chain) {
    amxc_var_t* iptables = amxo_parser_get_config(fw_get_parser(), "iptables_chains");
    const char* chain = GETP_CHAR(iptables, search_path);
    if(STRING_EMPTY(chain)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get chain, using default %s", default_chain);
        chain = default_chain;
    }
    return chain;
}

static const char* fw_host_translate_rule_flag(const fw_rule_flag_t flag) {
    const char* str_flag = "unknown";

    switch(flag) {
    case FW_RULE_FLAG_NEW:
        str_flag = "new";
        break;
    case FW_RULE_FLAG_MODIFIED:
        str_flag = "modified";
        break;
    case FW_RULE_FLAG_DELETED:
        str_flag = "deleted";
        break;
    case FW_RULE_FLAG_HANDLED:
        str_flag = "handled";
        break;
    case FW_RULE_FLAG_LAST:
        str_flag = "last (nothing)";
        break;
    }

    return str_flag;
}

bool fw_host_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag, const char* chain, const char* table, int index) {
    int ret = -1;

    if((flag == FW_RULE_FLAG_NEW) || (flag == FW_RULE_FLAG_MODIFIED)) {
        ret = fw_replace_rule(rule, index);
    } else if(flag == FW_RULE_FLAG_DELETED) {
        ret = fw_delete_rule(rule, index);
    }

    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to handle %s rule chain[%s] table[%s] index[%d]", fw_host_translate_rule_flag(flag), chain, table, index);
    } else {
        SAH_TRACEZ_INFO(ME, "Handled %s rule chain[%s] table[%s] index[%d]", fw_host_translate_rule_flag(flag), chain, table, index);
    }

    return (ret == 0);
}

/**
   @brief
   Activate the WAN Access

   @param[in] fw_gmap_device_t Gmap device internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool fw_host_activate_block(fw_gmap_device_t* gmap_device, bool ipv4) {
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    fw_folder_t* folder = NULL;
    bool res = false;
    const char* mac_address = NULL;
    const char* chain = NULL;
    const char* chain6 = NULL;

    when_null_trace(gmap_device, out, ERROR, "The gmap device is NULL.");
    mac_address = amxc_string_get(&(gmap_device->mac_addr), 0);
    when_str_empty_trace(mac_address, out, ERROR, "The MAC Address is an empty string.");

    folder = (ipv4) ? gmap_device->common.folder : gmap_device->common.folder6;

    fw_folder_set_feature(folder, FW_FEATURE_SOURCE_MAC);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "Failed to fetch rule");

    if(STRING_EMPTY(chain)) {
        chain = fw_get_chain("hosts_block", "FORWARD_Hosts_Block");
    }
    if(STRING_EMPTY(chain6)) {
        chain6 = fw_get_chain("hosts_block6", "FORWARD6_Hosts_Block");
    }
    fw_rule_set_chain(r, (ipv4) ? chain : chain6);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_source_mac_address(r, mac_address);
    fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP);
    fw_rule_set_table(r, "filter");
    fw_folder_push_rule(folder, r);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_MAC);
    if(s == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to fetch rule");
        goto out;
    }
    fw_folder_push_rule(folder, s);

    fw_folder_set_enabled(folder, true);
    res = true;
out:
    return res;
}

static void unfold_fw_host(fw_gmap_device_t* gmap_device, bool do_commit) {
    when_null(gmap_device, out);

    fw_folder_delete_rules(gmap_device->common.folder);
    fw_folder_delete_rules(gmap_device->common.folder6);
    fw_commit(fw_host_rule_callback);
    if(do_commit) {
        fw_apply();
    }

out:
    return;
}

static bool deactivate_fw_host(fw_gmap_device_t* gmap_device) {
    if(gmap_device->common.status == FIREWALL_ENABLED) {
        /* do not commit yet, wait for the activate/delete to commit it. */
        unfold_fw_host(gmap_device, false);
        gmap_device->common.status = FIREWALL_DISABLED;
    }
    return true;
}

static bool activate_fw_host(fw_gmap_device_t* gmap_device) {
    bool res = true;

    if(gmap_device->common.status != FIREWALL_ENABLED) {
        res = fw_host_activate_block(gmap_device, true);
        when_false_trace(res, exit, ERROR, "Failed to activate WAN Access rules.");

        if(fw_commit(fw_host_rule_callback) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to commit WAN Access rules for ipv4");
            goto exit;
        }

        res = fw_host_activate_block(gmap_device, false);
        when_false(res, exit);

        if(fw_commit(fw_host_rule_callback) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to commit WAN Access rules for ipv6");
            deactivate_fw_host(gmap_device);
            goto exit;
        }

        gmap_device->common.status = FIREWALL_ENABLED;
    }

    return true;
exit:
    SAH_TRACEZ_INFO(ME, "Clean Firewall rules.");
    unfold_fw_host(gmap_device, true);
    gmap_device->common.status = FIREWALL_ERROR;
    return false;
}

bool fw_host_implement_changes(amxc_htable_t* ht_fw_host) {
    bool result = false;
    amxc_htable_for_each(it_device, ht_fw_host) {
        fw_gmap_device_t* gmap_device = amxc_htable_it_get_data(it_device, fw_gmap_device_t, it);

        if(gmap_device == NULL) {
            continue;
        }

        if(gmap_device->flags & FW_DELETED) {
            SAH_TRACEZ_NOTICE(ME, "Delete WAN Access for MAC: %s", amxc_string_get(&(gmap_device->mac_addr), 0));
            deactivate_fw_host(gmap_device);
        } else if(gmap_device->flags & FW_NEW) {
            SAH_TRACEZ_NOTICE(ME, "Add new WAN Access for MAC: %s", amxc_string_get(&(gmap_device->mac_addr), 0));
            result = activate_fw_host(gmap_device);
            if(result) {
                gmap_device->flags = 0;
            }
        } else if(gmap_device->flags & FW_MODIFIED) {
            SAH_TRACEZ_NOTICE(ME, "Deactivate, activate WAN Access for MAC: %s", amxc_string_get(&(gmap_device->mac_addr), 0));
            deactivate_fw_host(gmap_device);
            result = activate_fw_host(gmap_device);
            if(result) {
                gmap_device->flags = 0;
            }
        }
        fw_apply();
    }
    return true;
}

void fw_host_delete_common_rules(common_t* common) {
    fw_folder_delete(&common->folder);
    fw_folder_delete(&common->folder6);
    fw_commit(fw_host_rule_callback);
    fw_apply();
}