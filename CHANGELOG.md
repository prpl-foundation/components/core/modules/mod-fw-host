# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.3 - 2023-02-23(15:09:09 +0000)

### Other

- Opensource component

## Release v0.1.2 - 2023-02-16(10:35:12 +0000)

### Other

- [mod_fw-host] opensource mod_fw-host module

## Release v0.1.1 - 2023-02-09(14:59:47 +0000)

### Fixes

- [prpl][amx] [Firewall] Implementation of WANAccess blocking

## Release v0.1.0 - 2023-02-09(10:14:24 +0000)

### New

- [prpl][amx] [Firewall] Implementation of WANAccess blocking

