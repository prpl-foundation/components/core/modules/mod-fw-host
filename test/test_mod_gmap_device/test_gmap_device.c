/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "fw_host_rules.h"
#include "fw_gmap_device.h"

#include "mock.h"
#include "test_common.h"
#include "test_gmap_device.h"

#define TEST_DATA_1_KEY "ID-4927463d-7b6c-4187-b84d-224586793f4f"
#define TEST_DATA_1_MAC_ADDRESS "6C:02:E0:0A:E7:95"

#define TEST_DATA_2_KEY "ID-361e9603-c23c-4c1a-b1cc-bfac0246f121"
#define TEST_DATA_2_MAC_ADDRESS "1C:39:47:59:66:09"

static amxc_var_t* create_var(const char* mac_address) {
    amxc_var_t* tmp = NULL;

    assert_non_null(mac_address);
    assert_string_not_equal(mac_address, "");
    assert_string_not_equal(mac_address, " ");

    amxc_var_new(&tmp);
    amxc_var_set_type(tmp, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, tmp, "PhysAddress", mac_address);

    return tmp;
}

int test_gmap_device_setup(UNUSED void** state) {
    mock_init(false);
    return 0;
}

int test_gmap_device_teardown(UNUSED void** state) {
    clean_all_gmap_devices();
    mock_cleanup();
    return 0;
}

void test_operations_gmap_device(UNUSED void** state) {
    amxc_var_t* var_1 = create_var(TEST_DATA_1_MAC_ADDRESS);
    amxc_var_t* var_2 = create_var(TEST_DATA_2_MAC_ADDRESS);
    fw_rule_t* rule_1_ipv4 = NULL;
    fw_rule_t* rule_1_ipv6 = NULL;
    fw_rule_t* bckp_rule_ipv4 = NULL;
    fw_rule_t* bckp_rule_ipv6 = NULL;
    fw_rule_t* rule_2_ipv4 = NULL;
    fw_rule_t* rule_2_ipv6 = NULL;
    fw_gmap_device_t* tmp = NULL;

    //Add a device to be blocked
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_1_MAC_ADDRESS, false);
    add_gmap_device(TEST_DATA_1_KEY, var_1);
    tmp = get_gmap_device(TEST_DATA_1_KEY);
    assert_non_null(tmp);
    assert_int_equal(tmp->common.status, FIREWALL_ENABLED);
    assert_int_equal(tmp->flags, 0);
    assert_string_equal(amxc_string_get(&(tmp->mac_addr), 0), TEST_DATA_1_MAC_ADDRESS);

    //Add a second device to be blocked
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_2_MAC_ADDRESS, false);
    add_gmap_device(TEST_DATA_2_KEY, var_2);
    tmp = get_gmap_device(TEST_DATA_2_KEY);
    assert_non_null(tmp);
    assert_int_equal(tmp->common.status, FIREWALL_ENABLED);
    assert_int_equal(tmp->flags, 0);
    assert_string_equal(amxc_string_get(&(tmp->mac_addr), 0), TEST_DATA_2_MAC_ADDRESS);

    //Change the MAC of the second device to the first device
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    bckp_rule_ipv4 = rule_1_ipv4;
    bckp_rule_ipv6 = rule_1_ipv6;
    rule_1_ipv4 = NULL;
    rule_1_ipv6 = NULL;
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_2_MAC_ADDRESS, false);
    add_gmap_device(TEST_DATA_1_KEY, var_2);
    fw_rule_delete(&bckp_rule_ipv4);
    fw_rule_delete(&bckp_rule_ipv6);
    tmp = get_gmap_device(TEST_DATA_1_KEY);
    assert_non_null(tmp);
    assert_int_equal(tmp->common.status, FIREWALL_ENABLED);
    assert_int_equal(tmp->flags, 0);
    assert_string_equal(amxc_string_get(&(tmp->mac_addr), 0), TEST_DATA_2_MAC_ADDRESS);

    //Change the MAC of the previous first device to the second device
    expect_delete_rule(rule_2_ipv4, 2);
    expect_delete_rule(rule_2_ipv6, 2);
    bckp_rule_ipv4 = rule_2_ipv4;
    bckp_rule_ipv6 = rule_2_ipv6;
    rule_2_ipv4 = NULL;
    rule_2_ipv6 = NULL;
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_1_MAC_ADDRESS, false);
    add_gmap_device(TEST_DATA_2_KEY, var_1);
    fw_rule_delete(&bckp_rule_ipv4);
    fw_rule_delete(&bckp_rule_ipv6);
    tmp = get_gmap_device(TEST_DATA_2_KEY);
    assert_non_null(tmp);
    assert_int_equal(tmp->common.status, FIREWALL_ENABLED);
    assert_int_equal(tmp->flags, 0);
    assert_string_equal(amxc_string_get(&(tmp->mac_addr), 0), TEST_DATA_1_MAC_ADDRESS);

    //Delete the first device
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    delete_gmap_device(TEST_DATA_1_KEY);
    tmp = get_gmap_device(TEST_DATA_1_KEY);
    assert_null(tmp);

    //Delete the second device
    expect_delete_rule(rule_2_ipv4, 1);
    expect_delete_rule(rule_2_ipv6, 1);
    delete_gmap_device(TEST_DATA_2_KEY);
    tmp = get_gmap_device(TEST_DATA_2_KEY);
    assert_null(tmp);

    fw_rule_delete(&rule_1_ipv4);
    fw_rule_delete(&rule_1_ipv6);
    fw_rule_delete(&rule_2_ipv4);
    fw_rule_delete(&rule_2_ipv6);
    amxc_var_delete(&var_1);
    amxc_var_delete(&var_2);
}

void test_clean_all_devices(UNUSED void** state) {
    amxc_var_t* var_1 = create_var(TEST_DATA_1_MAC_ADDRESS);
    amxc_var_t* var_2 = create_var(TEST_DATA_2_MAC_ADDRESS);
    fw_rule_t* rule_1_ipv4 = NULL;
    fw_rule_t* rule_1_ipv6 = NULL;
    fw_rule_t* rule_2_ipv4 = NULL;
    fw_rule_t* rule_2_ipv6 = NULL;
    fw_gmap_device_t* tmp = NULL;

    //Add a device to be blocked
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_1_MAC_ADDRESS, false);
    add_gmap_device(TEST_DATA_1_KEY, var_1);
    tmp = get_gmap_device(TEST_DATA_1_KEY);
    assert_non_null(tmp);
    assert_int_equal(tmp->common.status, FIREWALL_ENABLED);
    assert_int_equal(tmp->flags, 0);
    assert_string_equal(amxc_string_get(&(tmp->mac_addr), 0), TEST_DATA_1_MAC_ADDRESS);

    //Add a second device to be blocked
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_2_MAC_ADDRESS, false);
    add_gmap_device(TEST_DATA_2_KEY, var_2);
    tmp = get_gmap_device(TEST_DATA_2_KEY);
    assert_non_null(tmp);
    assert_int_equal(tmp->common.status, FIREWALL_ENABLED);
    assert_int_equal(tmp->flags, 0);
    assert_string_equal(amxc_string_get(&(tmp->mac_addr), 0), TEST_DATA_2_MAC_ADDRESS);

    //Delete all devices
    expect_delete_rule(rule_2_ipv4, 2);
    expect_delete_rule(rule_2_ipv6, 2);
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    clean_all_gmap_devices();
    tmp = get_gmap_device(TEST_DATA_1_KEY);
    assert_null(tmp);
    tmp = get_gmap_device(TEST_DATA_2_KEY);
    assert_null(tmp);

    fw_rule_delete(&rule_1_ipv4);
    fw_rule_delete(&rule_1_ipv6);
    fw_rule_delete(&rule_2_ipv4);
    fw_rule_delete(&rule_2_ipv6);
    amxc_var_delete(&var_1);
    amxc_var_delete(&var_2);
}