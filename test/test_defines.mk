MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include ../mocks/ ../common/)
MOCK_SRCDIR = $(realpath ../mocks/)
COMMON_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(MOCK_SRCDIR)/*.c) \
		  $(wildcard $(COMMON_SRCDIR)/*.c)

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks \
		  -fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
			-DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxb -lamxo \
		   -ldl -lpthread -lsahtrace -lgmap-client -lfwrules -lipat -levent

LDFLAGS += -g
