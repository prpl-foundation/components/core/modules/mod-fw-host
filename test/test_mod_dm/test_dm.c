/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "fw_host.h"
#include "mock.h"
#include "test_common.h"
#include "test_dm.h"
#include "fw_host_dm.h"

#include <amxd/amxd_transaction.h>

#define DEVICE_3_MAC_ADDRESS   "6C:02:E0:0A:E7:95"
#define DEVICE_3_IP            "192.168.1.10"
#define DEVICE_3_INTERFACE     "ethIntf-ETH3"
#define DEVICE_3_TAGS          "lan edev mac physical eth"
#define DEVICE_3_KEY           "ID-4927463d-7b6c-4187-b84d-224586793f4f"
#define DEVICE_3_DISCOVERY_SRC "bridge"

#define DEVICE_4_MAC_ADDRESS   "1C:39:47:59:66:09"
#define DEVICE_4_IP            "192.168.1.152"
#define DEVICE_4_INTERFACE     "ethIntf-ETH2"
#define DEVICE_4_TAGS          "lan edev mac physical eth ipv4 ipv6 dhcp"
#define DEVICE_4_KEY           "ID-361e9603-c23c-4c1a-b1cc-bfac0246f121"
#define DEVICE_4_DISCOVERY_SRC "bridge"

#define TEST_DATA_1_MAC_ADDRESS "00:39:47:59:66:09"
#define TEST_DATA_2_MAC_ADDRESS "FF:39:47:59:66:09"


static amxc_var_t* create_variant(const char* key, const char* mac, const char* ip, const char* intf,
                                  const char* discovery_src, const char* device_type, const char* tags) {
    amxc_var_t* tmp = NULL;

    amxc_var_new(&tmp);
    amxc_var_set_type(tmp, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_ALIAS, key);
    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_MAC, mac);
    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_IP, ip);
    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_INTF, intf);
    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_SRC, discovery_src);
    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_TYPE, device_type);
    amxc_var_add_key(cstring_t, tmp, DM_BLOCK_LIST_TAGS, tags);

    return tmp;
}

static void check_dm_number_of_block_list(int number_of_instances) {
    amxd_object_t* tmpl_block_list = NULL;
    uint32_t size_block_list = 0;

    handle_events();
    tmpl_block_list = amxd_dm_findf(get_dm(), "Firewall.X_PRPL-COM_WANAccess.BlockList.");
    assert_non_null(tmpl_block_list);
    size_block_list = (uint32_t) amxc_llist_size(&(tmpl_block_list->instances));
    assert_int_equal(size_block_list, number_of_instances);
}

static void check_dm_block_list(const char* key, const char* mac, const char* ip, const char* ip_src,
                                const char* intf, const char* discovery_src, const char* device_type, const char* tags) {
    amxd_object_t* tmpl_block_list = NULL;
    amxd_object_t* obj_block_list = NULL;
    char* tmp_string = NULL;

    handle_events();
    tmpl_block_list = amxd_dm_findf(get_dm(), "Firewall.X_PRPL-COM_WANAccess.BlockList.");
    assert_non_null(tmpl_block_list);
    obj_block_list = amxd_object_findf(tmpl_block_list, ".%s.", key);
    assert_non_null(obj_block_list);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_MAC, NULL);
    assert_string_equal(tmp_string, mac);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_IP, NULL);
    assert_string_equal(tmp_string, ip);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_IP_SRC, NULL);
    assert_string_equal(tmp_string, ip_src);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_INTF, NULL);
    assert_string_equal(tmp_string, intf);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_SRC, NULL);
    assert_string_equal(tmp_string, discovery_src);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_TYPE, NULL);
    assert_string_equal(tmp_string, device_type);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, DM_BLOCK_LIST_TAGS, NULL);
    assert_string_equal(tmp_string, tags);
    free(tmp_string);
}

int test_dm_setup(UNUSED void** state) {
    mock_init(true);
    handle_events();
    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    mock_cleanup();
    return 0;
}

void test_dm(UNUSED void** state) {
    amxc_var_t* device = NULL;
    int ret_value = -1;

    //Check prefix
    assert_string_equal(fw_get_vendor_prefix(get_parser()), "X_PRPL-COM_");

    //Add a device
    device = create_variant(DEVICE_3_KEY, DEVICE_3_MAC_ADDRESS, DEVICE_3_IP, DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS);
    ret_value = add_device_instance(get_dm(), get_parser(), DEVICE_3_KEY, device);
    assert_int_equal(ret_value, 0);
    amxc_var_delete(&device);

    check_dm_number_of_block_list(1);
    check_dm_block_list(DEVICE_3_KEY, DEVICE_3_MAC_ADDRESS, DEVICE_3_IP, "", DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS);

    //Add a second device
    device = create_variant(DEVICE_4_KEY, DEVICE_4_MAC_ADDRESS, DEVICE_4_IP, DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS);
    ret_value = add_device_instance(get_dm(), get_parser(), DEVICE_4_KEY, device);
    assert_int_equal(ret_value, 0);
    amxc_var_delete(&device);

    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_4_KEY, DEVICE_4_MAC_ADDRESS, DEVICE_4_IP, "", DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS);

    //Change the MAC of the first device
    device = create_variant(DEVICE_3_KEY, TEST_DATA_1_MAC_ADDRESS, DEVICE_3_IP, DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS);
    ret_value = add_device_instance(get_dm(), get_parser(), DEVICE_3_KEY, device);
    assert_int_equal(ret_value, 0);
    amxc_var_delete(&device);

    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_3_KEY, TEST_DATA_1_MAC_ADDRESS, DEVICE_3_IP, "", DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS);

    //Change the MAC of the second device
    device = create_variant(DEVICE_4_KEY, TEST_DATA_2_MAC_ADDRESS, DEVICE_4_IP, DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS);
    ret_value = change_device_instance(get_dm(), get_parser(), DEVICE_4_KEY, device);
    assert_int_equal(ret_value, 0);
    amxc_var_delete(&device);

    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_4_KEY, TEST_DATA_2_MAC_ADDRESS, DEVICE_4_IP, "", DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS);

    //Remove the first device
    ret_value = delete_device_instance(get_dm(), get_parser(), DEVICE_3_KEY);
    assert_int_equal(ret_value, 0);

    check_dm_number_of_block_list(1);

    //Remove the second device
    ret_value = delete_device_instance(get_dm(), get_parser(), DEVICE_4_KEY);
    assert_int_equal(ret_value, 0);

    check_dm_number_of_block_list(0);

    //Add the first device again
    device = create_variant(DEVICE_3_KEY, DEVICE_3_MAC_ADDRESS, DEVICE_3_IP, DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS);
    ret_value = add_device_instance(get_dm(), get_parser(), DEVICE_3_KEY, device);
    assert_int_equal(ret_value, 0);
    amxc_var_delete(&device);

    check_dm_number_of_block_list(1);
    check_dm_block_list(DEVICE_3_KEY, DEVICE_3_MAC_ADDRESS, DEVICE_3_IP, "", DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS);

    //Add the second device again
    device = create_variant(DEVICE_4_KEY, DEVICE_4_MAC_ADDRESS, DEVICE_4_IP, DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS);
    ret_value = add_device_instance(get_dm(), get_parser(), DEVICE_4_KEY, device);
    assert_int_equal(ret_value, 0);
    amxc_var_delete(&device);

    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_4_KEY, DEVICE_4_MAC_ADDRESS, DEVICE_4_IP, "", DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS);

    //Remove all the instances
    fw_host_clean_all_instances(get_dm(), get_parser());
    check_dm_number_of_block_list(0);
}
