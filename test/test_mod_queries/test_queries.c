/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "fw_host.h"
#include "mock.h"
#include "test_common.h"
#include "test_queries.h"

#include <amxd/amxd_transaction.h>

#define DEVICE_3_MAC_ADDRESS   "6C:02:E0:0A:E7:95"
#define DEVICE_3_INTERFACE     "ethIntf-ETH3"
#define DEVICE_3_TAGS          "lan edev mac physical eth"
#define DEVICE_3_KEY           "ID-4927463d-7b6c-4187-b84d-224586793f4f"
#define DEVICE_3_DISCOVERY_SRC "bridge"

#define DEVICE_4_MAC_ADDRESS   "1C:39:47:59:66:09"
#define DEVICE_4_INTERFACE     "ethIntf-ETH2"
#define DEVICE_4_TAGS          "lan edev mac physical eth ipv4 ipv6 dhcp"
#define DEVICE_4_KEY           "ID-361e9603-c23c-4c1a-b1cc-bfac0246f121"
#define DEVICE_4_DISCOVERY_SRC "bridge"

#define TEST_DATA_1_MAC_ADDRESS "00:39:47:59:66:09"
#define TEST_DATA_2_MAC_ADDRESS "FF:39:47:59:66:09"

static void change_device(int index, const char* key, const char* string) {
    amxd_trans_t trans;
    amxc_var_t* value = NULL;

    assert_true(index > 0);
    assert_int_equal(amxd_trans_init(&trans), 0);
    assert_int_equal(amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true), 0);
    amxd_trans_select_pathf(&trans, "Devices.Device.%d.", index);
    amxc_var_new(&value);
    amxc_var_set(cstring_t, value, string);
    amxd_trans_set_param(&trans, key, value);
    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    amxc_var_delete(&value);
    amxd_trans_clean(&trans);
    handle_events();
}

static void add_wanblock_to_tags(int index) {
    amxd_object_t* obj_device = NULL;
    amxc_string_t new_tags;
    char* tags = NULL;

    obj_device = amxd_dm_findf(get_dm(), "Devices.Device.%d.", index);
    assert_non_null(obj_device);
    tags = amxd_object_get_value(cstring_t, obj_device, "Tags", NULL);
    assert_non_null(tags);
    assert_string_not_equal(tags, "");
    assert_true(strstr(tags, "wanblock") == NULL);

    amxc_string_init(&new_tags, 0);
    amxc_string_setf(&new_tags, "%s wanblock", tags);
    change_device(index, "Tags", amxc_string_get(&new_tags, 0));
    amxc_string_clean(&new_tags);
    free(tags);
}

static void remove_wanblock_from_tags(int index) {
    amxd_object_t* obj_device = NULL;
    amxc_string_t new_tags;
    char* tags = NULL;

    obj_device = amxd_dm_findf(get_dm(), "Devices.Device.%d.", index);
    assert_non_null(obj_device);
    tags = amxd_object_get_value(cstring_t, obj_device, "Tags", NULL);
    assert_non_null(tags);
    assert_string_not_equal(tags, "");
    assert_true(strstr(tags, "wanblock") != NULL);

    amxc_string_init(&new_tags, 0);
    amxc_string_setf(&new_tags, "%s", tags);
    amxc_string_replace(&new_tags, " wanblock", "", UINT32_MAX);
    amxc_string_replace(&new_tags, "wanblock ", "", UINT32_MAX);
    amxc_string_replace(&new_tags, "wanblock", "", UINT32_MAX);
    change_device(index, "Tags", amxc_string_get(&new_tags, 0));
    amxc_string_clean(&new_tags);
    free(tags);
}

static void check_dm_number_of_block_list(int number_of_instances) {
    amxd_object_t* tmpl_block_list = NULL;
    uint32_t size_block_list = 0;

    handle_events();
    tmpl_block_list = amxd_dm_findf(get_dm(), "Firewall.X_PRPL-COM_WANAccess.BlockList.");
    assert_non_null(tmpl_block_list);
    size_block_list = (uint32_t) amxc_llist_size(&(tmpl_block_list->instances));
    assert_int_equal(size_block_list, number_of_instances);
}

static void check_dm_block_list(const char* key, const char* mac, const char* ip, const char* ip_src,
                                const char* intf, const char* discovery_src, const char* device_type, const char* tags) {
    amxd_object_t* tmpl_block_list = NULL;
    amxd_object_t* obj_block_list = NULL;
    char* tmp_string = NULL;

    handle_events();
    tmpl_block_list = amxd_dm_findf(get_dm(), "Firewall.X_PRPL-COM_WANAccess.BlockList.");
    assert_non_null(tmpl_block_list);
    obj_block_list = amxd_object_findf(tmpl_block_list, ".%s.", key);
    assert_non_null(obj_block_list);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "PhysAddress", NULL);
    assert_string_equal(tmp_string, mac);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "IPAddress", NULL);
    assert_string_equal(tmp_string, ip);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "IPAddressSource", NULL);
    assert_string_equal(tmp_string, ip_src);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "InterfaceName", NULL);
    assert_string_equal(tmp_string, intf);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "DiscoverySource", NULL);
    assert_string_equal(tmp_string, discovery_src);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "DeviceType", NULL);
    assert_string_equal(tmp_string, device_type);
    free(tmp_string);
    tmp_string = amxd_object_get_value(cstring_t, obj_block_list, "Tags", NULL);
    assert_string_equal(tmp_string, tags);
    free(tmp_string);
}

int test_queries_setup(UNUSED void** state) {
    mock_init(true);
    assert_int_equal(_fw_host_main(AMXO_START, get_dm(), get_parser()), 0);
    handle_events();
    return 0;
}

int test_queries_teardown(UNUSED void** state) {
    mock_cleanup();
    return 0;
}

void test_queries(UNUSED void** state) {
    fw_rule_t* rule_1_ipv4 = NULL;
    fw_rule_t* rule_1_ipv6 = NULL;
    fw_rule_t* bckp_rule_ipv4 = NULL;
    fw_rule_t* bckp_rule_ipv6 = NULL;
    fw_rule_t* rule_2_ipv4 = NULL;
    fw_rule_t* rule_2_ipv6 = NULL;

    //Add a device to be blocked
    expect_create_rule(&rule_1_ipv4, 1, DEVICE_3_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, DEVICE_3_MAC_ADDRESS, false);
    add_wanblock_to_tags(3);
    check_dm_number_of_block_list(1);
    check_dm_block_list(DEVICE_3_KEY, DEVICE_3_MAC_ADDRESS, "", "", DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS " wanblock");

    // //Add a second device to be blocked
    expect_create_rule(&rule_2_ipv4, 2, DEVICE_4_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, DEVICE_4_MAC_ADDRESS, false);
    add_wanblock_to_tags(4);
    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_4_KEY, DEVICE_4_MAC_ADDRESS, "", "", DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS " wanblock");

    // //Change the MAC of the first device
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    bckp_rule_ipv4 = rule_1_ipv4;
    bckp_rule_ipv6 = rule_1_ipv6;
    rule_1_ipv4 = NULL;
    rule_1_ipv6 = NULL;
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_1_MAC_ADDRESS, false);
    change_device(3, "PhysAddress", TEST_DATA_1_MAC_ADDRESS);
    fw_rule_delete(&bckp_rule_ipv4);
    fw_rule_delete(&bckp_rule_ipv6);
    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_3_KEY, TEST_DATA_1_MAC_ADDRESS, "", "", DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS " wanblock");

    // //Change the MAC of the second device
    expect_delete_rule(rule_2_ipv4, 2);
    expect_delete_rule(rule_2_ipv6, 2);
    bckp_rule_ipv4 = rule_2_ipv4;
    bckp_rule_ipv6 = rule_2_ipv6;
    rule_2_ipv4 = NULL;
    rule_2_ipv6 = NULL;
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_2_MAC_ADDRESS, false);
    change_device(4, "PhysAddress", TEST_DATA_2_MAC_ADDRESS);
    fw_rule_delete(&bckp_rule_ipv4);
    fw_rule_delete(&bckp_rule_ipv6);
    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_4_KEY, TEST_DATA_2_MAC_ADDRESS, "", "", DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS " wanblock");

    // //Unblock the first device
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    remove_wanblock_from_tags(3);
    check_dm_number_of_block_list(1);

    // //Unblock the second device
    expect_delete_rule(rule_2_ipv4, 1);
    expect_delete_rule(rule_2_ipv6, 1);
    remove_wanblock_from_tags(4);
    check_dm_number_of_block_list(0);

    //Add the first device to be blocked again
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_1_MAC_ADDRESS, false);
    add_wanblock_to_tags(3);
    check_dm_number_of_block_list(1);
    check_dm_block_list(DEVICE_3_KEY, TEST_DATA_1_MAC_ADDRESS, "", "", DEVICE_3_INTERFACE, DEVICE_3_DISCOVERY_SRC, "", DEVICE_3_TAGS " wanblock");

    //Add the third device to be blocked again
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_2_MAC_ADDRESS, false);
    add_wanblock_to_tags(4);
    check_dm_number_of_block_list(2);
    check_dm_block_list(DEVICE_4_KEY, TEST_DATA_2_MAC_ADDRESS, "", "", DEVICE_4_INTERFACE, DEVICE_4_DISCOVERY_SRC, "", DEVICE_4_TAGS " wanblock");

    //Close the module
    expect_delete_rule(rule_2_ipv4, 2);
    expect_delete_rule(rule_2_ipv6, 2);
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    amxp_sigmngr_emit_signal(&(get_dm()->sigmngr), "app:stop", NULL);
    handle_events();
    assert_int_equal(_fw_host_main(AMXO_STOP, get_dm(), get_parser()), 0);
    check_dm_number_of_block_list(0);

    fw_rule_delete(&rule_1_ipv4);
    fw_rule_delete(&rule_1_ipv6);
    fw_rule_delete(&rule_2_ipv4);
    fw_rule_delete(&rule_2_ipv6);
}
