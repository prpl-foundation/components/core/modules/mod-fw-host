/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <gmap/gmap.h>

#include "mock.h"
#include "gmap_mock.h"

static gmap_query_cb_t callback_function = NULL;

/**
   @brief
   Copy the data from the object to the variant.

   @param[in] object Object.
   @param[out] var Variant.
 */
static void obj_data_to_var(amxd_object_t* object, amxc_var_t* var) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    uint32_t index = 1;
    uint32_t inst_count = 0;
    amxc_string_t* str_index;
    amxd_object_type_t obj_type;

    amxc_string_new(&str_index, 0);

    assert_non_null(object);
    assert_non_null(var);

    obj_type = amxd_object_get_type(object);
    if(obj_type == amxd_object_template) {
        inst_count = amxd_object_get_instance_count(object);
        if(inst_count > 0) {
            amxc_llist_for_each(it, (&object->instances)) {
                tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
                index = amxd_object_get_index(tmp_obj);
                amxc_string_setf(str_index, "%u", index);
                tmp_var = amxc_var_add_key(amxc_htable_t, var, str_index->buffer, NULL);
                obj_data_to_var(tmp_obj, tmp_var);
                tmp_obj = NULL;
            }
        }
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            if((amxc_var_type_of(tmp_var) != AMXC_VAR_ID_HTABLE) && \
               (amxc_var_type_of(tmp_var) != AMXC_VAR_ID_LIST)) {
                amxc_var_set_pathf(var, tmp_var, AMXC_VAR_FLAG_AUTO_ADD | AMXC_VAR_FLAG_COPY, "%s", param_name);
            }

            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            tmp_var = amxc_var_add_key(amxc_htable_t, var, obj_name, NULL);
            obj_data_to_var(tmp_obj, tmp_var);
            tmp_obj = NULL;
        }
    }
    amxc_string_delete(&str_index);
}

static void call_function(amxd_object_t* obj_device, gmap_query_action_t action) {
    char* key = NULL;
    amxc_var_t* device = NULL;

    assert_non_null(obj_device);
    key = amxd_object_get_value(cstring_t, obj_device, "Alias", NULL);
    amxc_var_new(&device);
    amxc_var_set_type(device, AMXC_VAR_ID_HTABLE);
    obj_data_to_var(obj_device, device);
    callback_function(NULL, key, device, action);
    amxc_var_delete(&device);
    free(key);
}

static void check_all_devices(void) {
    amxd_object_t* templ_device = amxd_dm_findf(get_dm(), "Devices.Device.");
    amxd_object_t* obj_device = NULL;

    amxd_object_for_each(instance, it, templ_device) {
        char* tags = NULL;

        obj_device = amxc_container_of(it, amxd_object_t, it);
        tags = amxd_object_get_value(cstring_t, obj_device, "Tags", NULL);
        if(strstr(tags, "wanblock") != NULL) {
            call_function(obj_device, gmap_query_expression_start_matching);
        }
        free(tags);
    }
}

gmap_query_t* gmap_query_open_ext(UNUSED const char* expression, UNUSED const char* name, gmap_query_cb_t fn, UNUSED void* user_data) {
    gmap_query_t* gmap_query = NULL;

    callback_function = fn;
    gmap_query = (gmap_query_t*) calloc(1, sizeof(gmap_query_t));
    check_all_devices();

    return gmap_query;
}

void gmap_query_close(gmap_query_t* query) {
    when_null(query, exit);
    free(query);

exit:
    return;
}

void _gmap_mock_query(UNUSED const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    amxd_object_t* obj_device = amxd_dm_signal_get_object(get_dm(), event_data);
    const amxc_var_t* parameters = NULL;
    const amxc_var_t* tmp_var = NULL;
    const char* old_tags = NULL;
    const char* new_tags = NULL;
    bool new_has_tag = false;
    bool old_has_tag = false;
    gmap_query_action_t action = gmap_query_device_updated;

    assert_non_null(event_data);
    assert_non_null(obj_device);
    parameters = GET_ARG(event_data, "parameters");
    assert_non_null(parameters);

    tmp_var = GET_ARG(parameters, "Tags");
    if(tmp_var != NULL) {
        new_tags = GET_CHAR(tmp_var, "to");
        assert_string_not_equal(new_tags, "");
        old_tags = GET_CHAR(tmp_var, "from");
        assert_string_not_equal(old_tags, "");

        if(strstr(new_tags, "wanblock") != NULL) {
            new_has_tag = true;
        }
        if(strstr(old_tags, "wanblock") != NULL) {
            old_has_tag = true;
        }
        if(new_has_tag && !old_has_tag) {
            action = gmap_query_expression_start_matching;
        } else if((new_has_tag & !old_has_tag) || (!new_has_tag & old_has_tag)) {
            action = gmap_query_expression_stop_matching;
        }
    }
    call_function(obj_device, action);

    return;
}