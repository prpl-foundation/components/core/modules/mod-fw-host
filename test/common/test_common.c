#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <regex.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <event2/event.h>
#include <yajl/yajl_gen.h>

#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "mock.h"
#include "test_common.h"

static struct event_base* base = NULL;
static int timeout = 0;

void expect_create_rule(fw_rule_t** rule, int index, const char* mac_address, bool ipv4) {
    fw_rule_delete(rule);
    assert_int_equal(mock_fw_rule_new(rule), 0);
    assert_int_equal(fw_rule_set_table(*rule, "filter"), 0);
    if(ipv4) {
        assert_int_equal(fw_rule_set_chain(*rule, "FORWARD_Hosts_Block"), 0);
    } else {
        assert_int_equal(fw_rule_set_chain(*rule, "FORWARD6_Hosts_Block"), 0);
    }
    assert_int_equal(fw_rule_set_ipv4(*rule, ipv4), 0);
    assert_int_equal(fw_rule_set_source_mac_address(*rule, mac_address), 0);
    assert_int_equal(fw_rule_set_target_policy(*rule, FW_RULE_POLICY_DROP), 0);
    assert_int_equal(fw_rule_set_enabled(*rule, true), 0);

    expect_fw_replace_rule(*rule, index);
}

void expect_delete_rule(fw_rule_t* rule, int index) {
    expect_fw_delete_rule(rule, index);
}

static void test_timer_cb(UNUSED amxp_timer_t* t, void* priv) {
    int* timeout = (int*) priv;
    *timeout = 1;
    print_message("Test time out reached! (%s:%d)\n", __func__, __LINE__);
    event_base_loopbreak(base);
}

static void el_signal_timers(UNUSED evutil_socket_t fd,
                             UNUSED short event,
                             UNUSED void* arg) {
    amxp_timers_calculate();
    amxp_timers_check();
    event_base_loopbreak(base);
}


void wait_for_sigalarm(int seconds) {
    amxp_timer_t* test_timer = NULL;
    struct event* signal_alarm = NULL;

    timeout = 0;

    amxp_timer_new(&test_timer, test_timer_cb, &timeout);
    amxp_timer_start(test_timer, seconds * 1000);

    base = event_base_new();
    assert_non_null(base);

    signal_alarm = evsignal_new(base,
                                SIGALRM,
                                el_signal_timers,
                                NULL);
    event_add(signal_alarm, NULL);

    event_base_dispatch(base);

    amxp_timer_delete(&test_timer);
    event_del(signal_alarm);
    event_base_free(base);
    base = NULL;
    free(signal_alarm);

    assert_int_equal(timeout, 0);
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}