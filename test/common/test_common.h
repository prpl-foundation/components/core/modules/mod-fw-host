#ifndef __TEST_COMMON_H__
#define __TEST_COMMON_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

void expect_create_rule(fw_rule_t** rule, int index, const char* mac_address, bool ipv4);
void expect_delete_rule(fw_rule_t* rule, int index);

void wait_for_sigalarm(int seconds);

#ifdef __cplusplus
}
#endif

#endif // __TEST_COMMON_H__