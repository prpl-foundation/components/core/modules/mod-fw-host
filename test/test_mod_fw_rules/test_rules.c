/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "mock.h"
#include "test_common.h"
#include "test_rules.h"
#include "fw_host_rules.h"
#include "fw_gmap_device.h"

#define TEST_DATA_1_KEY "ID-4927463d-7b6c-4187-b84d-224586793f4f"
#define TEST_DATA_1_MAC_ADDRESS "6C:02:E0:0A:E7:95"

#define TEST_DATA_2_KEY "ID-361e9603-c23c-4c1a-b1cc-bfac0246f121"
#define TEST_DATA_2_MAC_ADDRESS "1C:39:47:59:66:09"

static amxc_htable_t* ht_fw_host = NULL;

static fw_gmap_device_t* create_gmap_device(const char* mac_address) {
    fw_gmap_device_t* tmp = NULL;

    assert_non_null(mac_address);
    assert_string_not_equal(mac_address, "");
    assert_string_not_equal(mac_address, " ");

    tmp = calloc(1, sizeof(fw_gmap_device_t));
    amxc_string_init(&(tmp->mac_addr), 0);
    amxc_string_setf(&(tmp->mac_addr), "%s", mac_address);
    fw_folder_new(&tmp->common.folder);
    fw_folder_new(&tmp->common.folder6);
    tmp->flags |= FW_NEW;
    tmp->common.status = FIREWALL_DISABLED;

    return tmp;
}

static void change_gmap_device(fw_gmap_device_t* gmap_device, const char* mac_address) {
    assert_non_null(gmap_device);
    assert_non_null(mac_address);
    assert_string_not_equal(mac_address, "");
    assert_string_not_equal(mac_address, " ");

    amxc_string_setf(&(gmap_device->mac_addr), "%s", mac_address);
    gmap_device->flags |= FW_MODIFIED;
}

static void clean_gmap_device(fw_gmap_device_t* gmap_device) {
    assert_non_null(gmap_device);
    fw_host_delete_common_rules(&(gmap_device->common));
    amxc_string_clean(&(gmap_device->mac_addr));
    amxc_htable_it_clean(&gmap_device->it, NULL);
    free(gmap_device);
    gmap_device = NULL;
}

static void mock_clean_all_gmap_devices(void) {
    amxc_htable_for_each(it_device, ht_fw_host) {
        fw_gmap_device_t* gmap_device = amxc_htable_it_get_data(it_device, fw_gmap_device_t, it);
        clean_gmap_device(gmap_device);
    }
    amxc_htable_delete(&ht_fw_host, NULL);
    ht_fw_host = NULL;
}

static fw_gmap_device_t* mock_get_gmap_device(const cstring_t key) {
    fw_gmap_device_t* device = NULL;

    assert_non_null(key);
    assert_string_not_equal(key, "");
    assert_string_not_equal(key, " ");
    assert_non_null(ht_fw_host);

    device = amxc_htable_it_get_data(amxc_htable_get(ht_fw_host, key), fw_gmap_device_t, it);
    return device;
}

static void mock_add_gmap_device(const cstring_t key, const char* mac_address) {
    amxc_htable_it_t* it_device = NULL;
    fw_gmap_device_t* gmap_device = NULL;

    assert_non_null(key);
    assert_string_not_equal(key, "");
    assert_string_not_equal(key, " ");
    assert_non_null(mac_address);
    assert_string_not_equal(mac_address, "");
    assert_string_not_equal(mac_address, " ");

    if(ht_fw_host == NULL) {
        amxc_htable_new(&ht_fw_host, 1);
    }

    it_device = amxc_htable_get(ht_fw_host, key);
    if(it_device == NULL) {
        gmap_device = create_gmap_device(mac_address);
        assert_non_null(gmap_device);
        amxc_htable_insert(ht_fw_host, key, &gmap_device->it);
    } else {
        gmap_device = amxc_htable_it_get_data(it_device, fw_gmap_device_t, it);
        assert_non_null(gmap_device);
        change_gmap_device(gmap_device, mac_address);
    }
    fw_host_implement_changes(ht_fw_host);
}

static void mock_delete_gmap_device(const cstring_t key) {
    fw_gmap_device_t* gmap_device = NULL;

    assert_non_null(ht_fw_host);
    assert_non_null(key);
    assert_string_not_equal(key, "");
    assert_string_not_equal(key, " ");

    gmap_device = mock_get_gmap_device(key);
    assert_non_null(gmap_device);
    gmap_device->flags |= FW_DELETED;
    fw_host_implement_changes(ht_fw_host);

    clean_gmap_device(gmap_device);

    if(amxc_htable_is_empty(ht_fw_host)) {
        clean_all_gmap_devices();
    }
}

int test_rules_setup(UNUSED void** state) {
    mock_init(false);
    return 0;
}

int test_rules_teardown(UNUSED void** state) {
    mock_clean_all_gmap_devices();
    mock_cleanup();
    return 0;
}

void test_fw_implement_changes(UNUSED void** state) {
    fw_rule_t* rule_1_ipv4 = NULL;
    fw_rule_t* rule_1_ipv6 = NULL;
    fw_rule_t* bckp_rule_ipv4 = NULL;
    fw_rule_t* bckp_rule_ipv6 = NULL;
    fw_rule_t* rule_2_ipv4 = NULL;
    fw_rule_t* rule_2_ipv6 = NULL;

    //Add a device to be blocked
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_1_MAC_ADDRESS, false);
    mock_add_gmap_device(TEST_DATA_1_KEY, TEST_DATA_1_MAC_ADDRESS);

    //Add a second device to be blocked
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_2_MAC_ADDRESS, false);
    mock_add_gmap_device(TEST_DATA_2_KEY, TEST_DATA_2_MAC_ADDRESS);

    //Change the MAC of the second device to the first device
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    bckp_rule_ipv4 = rule_1_ipv4;
    bckp_rule_ipv6 = rule_1_ipv6;
    rule_1_ipv4 = NULL;
    rule_1_ipv6 = NULL;
    expect_create_rule(&rule_1_ipv4, 1, TEST_DATA_2_MAC_ADDRESS, true);
    expect_create_rule(&rule_1_ipv6, 1, TEST_DATA_2_MAC_ADDRESS, false);
    mock_add_gmap_device(TEST_DATA_1_KEY, TEST_DATA_2_MAC_ADDRESS);
    fw_rule_delete(&bckp_rule_ipv4);
    fw_rule_delete(&bckp_rule_ipv6);

    //Change the MAC of the previous first device to the second device
    expect_delete_rule(rule_2_ipv4, 2);
    expect_delete_rule(rule_2_ipv6, 2);
    bckp_rule_ipv4 = rule_2_ipv4;
    bckp_rule_ipv6 = rule_2_ipv6;
    rule_2_ipv4 = NULL;
    rule_2_ipv6 = NULL;
    expect_create_rule(&rule_2_ipv4, 2, TEST_DATA_1_MAC_ADDRESS, true);
    expect_create_rule(&rule_2_ipv6, 2, TEST_DATA_1_MAC_ADDRESS, false);
    mock_add_gmap_device(TEST_DATA_2_KEY, TEST_DATA_1_MAC_ADDRESS);
    fw_rule_delete(&bckp_rule_ipv4);
    fw_rule_delete(&bckp_rule_ipv6);

    //Delete the first device
    expect_delete_rule(rule_1_ipv4, 1);
    expect_delete_rule(rule_1_ipv6, 1);
    mock_delete_gmap_device(TEST_DATA_1_KEY);

    //Delete the second device
    expect_delete_rule(rule_2_ipv4, 1);
    expect_delete_rule(rule_2_ipv6, 1);
    mock_delete_gmap_device(TEST_DATA_2_KEY);

    fw_rule_delete(&rule_1_ipv4);
    fw_rule_delete(&rule_1_ipv6);
    fw_rule_delete(&rule_2_ipv4);
    fw_rule_delete(&rule_2_ipv6);
}
